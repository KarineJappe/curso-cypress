describe('Tickets', () => {
  beforeEach(() => cy.visit('https://bit.ly/2XSuwCW') );

  it("fills all the text input fields", () => {
    const firstName = "Karine";
    const lasttName = "Jappe";

    cy.get("input#first-name").type(firstName);
    cy.get("input#last-name").type(lasttName);
    cy.get("input#email").type("karine@mailinator.com");
    cy.get("textarea#requests").type("Vegetarian");
    cy.get("input#signature").type(`${firstName} ${lasttName}`);
  });

  it("select two tickets", () => {
    cy.get("select#ticket-quantity").select("2");
  });

  it("select 'vip' ticket type", () => {
    cy.get("input#vip").check();
  });

  it("selects 'social media' checkbox", () => {
    cy.get("input#social-media").check();
  });

  it("selects 'friend', and 'publication', then uncheck 'friend'", () => {
    cy.get("input#friend").check();
    cy.get("input#publication").check();
    cy.get("input#friend").uncheck();
  });

  it("has 'TICKETBOX' header's heading", () => {
    cy.get("header h1").should("contain", "TICKETBOX");
  });

  it("alerts on invalid email", () => {
    cy.get("input#email")
      .as("email")
      .type("karine-mailinator.com");

    cy.get("input#email.invalid").should("exist");

    cy.get("@email")
      .clear()
      .type("karine@mailinator.com");

    cy.get("input#email.invalid").should("not.exist");
  });

  it("fills and reset the form", () => {
    const firstName = "Karine";
    const lasttName = "Jappe";
    const fullName = `${firstName} ${lasttName}`;

    cy.get("input#first-name").type(firstName);
    cy.get("input#last-name").type(lasttName);
    cy.get("input#email").type("karine@mailinator.com");
    cy.get("select#ticket-quantity").select("2");
    cy.get("input#vip").check();
    cy.get("input#friend").check();
    cy.get("textarea#requests").type("IPA beer");
    
    cy.get(".agreement p").should(
      "contain",
      `I, ${fullName}, wish to buy 2 VIP tickets`
    );

    cy.get("input#agree").click();
    cy.get("input#signature").type(fullName);

    cy.get("button[type=submit]")
      .as("submitButton")
      .should("not.be.disabled");

    cy.get("button[type=reset]").click();

    cy.get("@submitButton").should("be.disabled");
  });

  it("fills mandatory fields using support command", () => {
    const customer = {
      firstName: "Joao",
      lasttName: "Silva",
      email: "joaosilva@example.com"
    };

    cy.fillMandatoryFields(customer);

    cy.get("button[type=submit]")
      .as("submitButton")
      .should("not.be.disabled");

    cy.get("input#agree").uncheck();

    cy.get("@submitButton").should("be.disabled");
  });
});